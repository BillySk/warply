import { createStackNavigator, createAppContainer } from 'react-navigation';
import Home from './Home.js';
import TabScreen from './TabScreen.js';


const AppNavigator = createStackNavigator({
  Home: { screen: Home },
  Tabs: { screen: TabScreen}
});

const AppNavigatorContainer = createAppContainer(AppNavigator);


export default AppNavigatorContainer;