import React from 'react';
import {View,Text, ScrollView, StyleSheet} from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import { connect } from 'react-redux';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';

class PoiScreen extends React.Component {
		
	render() {
		let datasource=this.props.screenProps.POIs;
		return (
			<ScrollView style={styles.container}>
				{datasource.map(i =>
				<View key={i.id} style={{ flex: 1, justifyContent: 'center', paddingLeft:10 }}>
					<Text>{i.address} distance: {(!isNaN(i.distance))?i.distance / 1000: "unknown"} km{"\n"}</Text>
				</View>
				)}
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
	flex: 1,
	paddingTop: 15,
	backgroundColor: '#fff'
	},
});

class MapScreen extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			region: {
				latitude: this.props.screenProps.userLocation.coords.latitude,
				longitude: this.props.screenProps.userLocation.coords.longitude,
				latitudeDelta: 0.04,
				longitudeDelta: 0.05,
			}
		}
	}

	render() {
		return (
			<MapView
				style= {mapStyles.map}
				showUserLocation = {true}
				provider={PROVIDER_GOOGLE}
				initialRegion={this.state.region}
			>
				{this.props.screenProps.POIs.map ( poi => {
					if (poi.latitude && poi.longitude) {
						return <Marker key={poi.id} coordinate={{latitude: +poi.latitude, longitude: +poi.longitude}} title={poi.address} />;
					}
				})}
			</MapView>
		);
	}
}

const mapStyles = StyleSheet.create({ map: { position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, }, });

const TabNavigator = createBottomTabNavigator({
	POIs: PoiScreen,
	Map: MapScreen,
});
const TabScreen = createAppContainer(TabNavigator)

const mapStateToProps = (state) => {
const {userLocation, POIs} = state.info
return {userLocation, POIs}
}

const mergeProps = (state, dispatch, ownProps) => {
return ({
	...ownProps,
	screenProps: {
		...ownProps.screenProps,
		...state,
		...dispatch,
	}
})
}

export default connect(mapStateToProps,null, mergeProps)(TabScreen);