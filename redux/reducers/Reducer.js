import { combineReducers } from 'redux';
import { ADD_LOCATION, ADD_POIS } from '../actions/ActionTypes.js';

const INITIAL_STATE = {
    POIs : [],
    userLocation : null
};

const reduser = (state = INITIAL_STATE , action) => {
    let {userLocation, POIs} = state;
    let newState;
    switch (action.type) {
        case ADD_LOCATION:
            userLocation = action.payload;
            newState = {userLocation, POIs};
            return newState;
        case ADD_POIS:
            POIs = action.payload;
            newState = {userLocation, POIs};
            return newState;
        default:
            return state;
    }
};


export default combineReducers({
    info: reduser,
});