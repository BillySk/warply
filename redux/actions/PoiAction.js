import { ADD_POIS } from '../actions/ActionTypes.js';

export const addPOIs = pois => (
    {
        type: ADD_POIS,
        payload: pois,
    }
);