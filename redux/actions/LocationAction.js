import { ADD_LOCATION } from '../actions/ActionTypes.js';

export const addLocation = location => (
    {
        type: ADD_LOCATION,
        payload: location,
    }
);