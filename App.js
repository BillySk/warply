import React from 'react';
import { StyleSheet} from 'react-native';
import AppNavigatorContainer from './AppNavigator.js';
import reducer from './redux/reducers/Reducer.js';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

const store = createStore(reducer);

export default class App extends React.Component {  
  render(){
      return (
        <Provider store={store}>
           <AppNavigatorContainer/>
        </Provider>
    );
  }
}