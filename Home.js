// Home.js
import React from 'react';
import {
    View,
    Text,
    Button,
    Modal
} from 'react-native';
import {addLocation} from './redux/actions/LocationAction.js';
import {addPOIs} from './redux/actions/PoiAction.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import { getDistance } from 'geolib';


class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			modalVisible: false
		}
  }
  componentWillMount() {
    this._getLocationAsync();
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        modalVisible: true,
      });
    }
    let location = await Location.getCurrentPositionAsync({enableHighAccuracy:true});
    this.props.addLocation(location);
    this.getPois();
  };

  sortPois(poiList) {
    poiList.sort((a, b) => {
      a.distance = getDistance(this.props.userLocation.coords, a);
      b.distance = getDistance(this.props.userLocation.coords, b);
      return a.distance > b.distance ? 1 : -1;
    })
    return poiList;
  } 
  
  getPois() {
    return fetch('https://warply.s3.amazonaws.com/data/test_pois.json')
      .then((response) => response.json())
      .then((responseJson) => {
        const pois = this.sortPois(responseJson);
        this.props.addPOIs( pois);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    return (
      <View style = {{ justifyContent: 'center', alignItems: 'center', flex: 1}}>
        <Text>Hello from Home screen.</Text>
        <Button
          onPress={() => this.props.navigation.navigate('Tabs')}
          title="View next screen"
        />
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose = {() => { console.log("Modal has been closed.") } }>
            <Text style = {{ top: 25 }}>Location should be shared, some services may not work correctly!</Text>
            <Button  style = {{ top: 65 }}
            title="Close" onPress = {() => {
                     this.setState({
                      modalVisible: false
                    });}}></Button>
        </Modal>
      </View>
    )
  }
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    addLocation, addPOIs
  }, dispatch)
)

const mapStateToProps = (state) => {
  const { userLocation, POIs } = state.info;
  return { userLocation, POIs }
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);